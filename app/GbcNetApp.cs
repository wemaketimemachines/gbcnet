﻿using System;
using System.IO;

using gbcNET.core;
using gbcNET.cpu;
using Microsoft.VisualBasic;
using static gbcNET.GbcNetLogger;

namespace gbcNET {
    static class GbcNetApp {
        
        public static byte[] GbcMainRam = new byte[8192];
        
        // Create lookup table for bin to hex conversion (Debug functionality)
        // private static readonly uint[] _lookup32 = ConversionUtils.CreateLookup32();
        
        static void Main(string[] args) {
            InitLogger(LogLevel.Trace, "F:/projects/gbemu/log.txt");

            ColorPalette greyScalePalette =
                new ColorPalette(new[] {new RGB(255), new RGB(169), new RGB(84), new RGB(0)}) {name = "Greyscale"};
            ColorConfig colorConfig = new ColorConfig(greyScalePalette);
            VideoConfig videoConfig = new VideoConfig { Vsync = true, ScalingFactor = 5 };
            AppConfig appConfig = new AppConfig { ColorConfig = colorConfig, VideoConfig = videoConfig };

            if (!File.Exists(args[0])) {
                LogFatal("File not found: " + args[0]);
                LogFatal("Exiting - error code 1");
                Environment.Exit(1);
            }
            
            LogInfo("Load: " + args[0]);
            GbcCore gbcCore = new GbcCore(File.ReadAllBytes(args[0]));

            if (gbcCore.RomCache.Length < 1) {
                LogFatal("Empty game rom!");
                LogFatal("Exiting, error code 2");
                Environment.Exit(2);
            }

            LogInfo("Loaded rom: " + gbcCore.RomCache.Length + " bytes");

            GbcCpu cpu = new GbcCpu();
            
            cpu.Registers.BC[0] = 15;
            cpu.Registers.BC[1] = 22;

            ushort bcValue = cpu.Registers.GetBC();
            Console.WriteLine(bcValue);

            byte bValue = cpu.Registers.BC[0];
            byte cValue = cpu.Registers.BC[1];
            
            Console.WriteLine(bValue + " " + cValue);
            
            cpu.Registers.SetBC(bcValue);
            
            Console.WriteLine(cpu.Registers.BC[0] + " " + cpu.Registers.BC[1]);


            // MainWindow mainWindow = new MainWindow(videoConfig, gbcCore.RomHeader.GameTitle);
            // mainWindow.Show();

        }
    }
}