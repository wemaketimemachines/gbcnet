namespace gbcNET {
    public class ColorConfig {
        
        public ColorPalette[] ColorPalettes;

        public ColorConfig(ColorPalette[] palettes) {
            ColorPalettes = palettes;
        }

        public ColorConfig(ColorPalette palette) {
            ColorPalettes = new[] { palette };
        }
    }

    public class ColorPalette {
        public string name;
        public RGB[] colors;

        public ColorPalette(RGB color1, RGB color2, RGB color3, RGB color4) {
            colors = new[] {color1, color2, color3, color4};
        }

        public ColorPalette(RGB[] colors) {
            this.colors = colors;
        }
    }
}
