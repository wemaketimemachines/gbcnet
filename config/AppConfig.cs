namespace gbcNET {
    public class AppConfig {

        public VideoConfig VideoConfig;
        public ControllerConfig[] ControllerConfigs;
        public ColorConfig ColorConfig;
        public string[] RecentFilePaths;
        public Vector2Int WindowOrigin;
        public string LogFilePath;

    }
}
