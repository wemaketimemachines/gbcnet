namespace gbcNET {
    public class VideoConfig {
        public bool Vsync;
        public int FpsLimiter;
        public uint ScalingFactor;
    }
}
