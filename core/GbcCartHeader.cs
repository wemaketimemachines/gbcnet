// Cart type reference:
// 00  ROM ONLY                 13  MBC3+RAM+BATTERY
// 01  MBC1                     15  MBC4
// 02  MBC1+RAM                 16  MBC4+RAM
// 03  MBC1+RAM+BATTERY         17  MBC4+RAM+BATTERY
// 05  MBC2                     19  MBC5
// 06  MBC2+BATTERY             1A  MBC5+RAM
// 08  ROM+RAM                  1B  MBC5+RAM+BATTERY
// 09  ROM+RAM+BATTERY          1C  MBC5+RUMBLE
// 0B  MMM01                    1D  MBC5+RUMBLE+RAM
// 0C  MMM01+RAM                1E  MBC5+RUMBLE+RAM+BATTERY
// 0D  MMM01+RAM+BATTERY        FC  POCKET CAMERA
// 0F  MBC3+TIMER+BATTERY       FD  BANDAI TAMA5
// 10  MBC3+TIMER+RAM+BATTERY   FE  HuC3
// 11  MBC3                     FF  HuC1+RAM+BATTERY
// 12  MBC3+RAM

using System;
using System.Text;
using static gbcNET.GbcNetLogger;

namespace gbcNET.core {
    public class GbcCartHeader {

        public string GameTitle;
        public byte CartType; // Address 147h / 327d - specifies hardware requirements
        public byte RomSizeFlag;
        public byte RamSizeFlag;

        public int MBC = 0; // Memory bank controller
        public bool cartRam = false;
        public bool cartBattery = false;
        public bool cartTimer = false;
        public bool cartRumble = false;
        public bool cartCamera = false;
        public bool MMM01 = false;
        public bool Tama5 = false;
        public bool HuC3 = false;
        public bool HuC1 = false;

        public GbcCartHeader(byte[] romContents) {

            StringBuilder builder = new StringBuilder();

            byte[] titleBytes = romContents[308..323];
            int byteCountAccumulator = 0;
            foreach (var character in titleBytes) {
                if (character != 0) {
                    byteCountAccumulator++;
                } else {
                    break;
                }
            }

            GameTitle = Encoding.GetEncoding("ISO-8859-1").GetString(titleBytes, 0, byteCountAccumulator);
            CartType = romContents[327];
            RomSizeFlag = romContents[328];
            RamSizeFlag = romContents[329];
            
            HardwareCheck(CartType);
            
            LogTrace("Parsing rom ----------------");
            LogTrace("Game title: " + GameTitle);
            LogTrace("Cart type code: " + CartType);
            LogTrace("Rom size flag: " + RomSizeFlag);
            LogTrace("Ram size flag " + RamSizeFlag);
            LogTrace("MBC: " + MBC);
            LogTrace("Cart RAM: " + cartRam);
            LogTrace("Cart Battery: " + cartBattery);
            LogTrace("Cart Timer: " + cartTimer);
            LogTrace("Cart Rumble: " + cartRumble);
            LogTrace("Cart Camera:" + cartCamera);
            LogTrace("MMM01: " + MMM01);
            LogTrace("Tama5: " + Tama5);
            LogTrace("HuC3: " + HuC3);
            LogTrace("HuC1: " + HuC1);
            
        }
        
        private void HardwareCheck(byte cartType) {
            
            switch (cartType) {
                case 0: //00h
                    // rom only no-op
                    break;
                
                case 1: //01h
                    MBC = 1;
                    break;
                
                case 2: //02h
                    MBC = 1;
                    cartRam = true;
                    break;
                
                case 3: //03h
                    MBC = 1;
                    cartRam = true;
                    cartBattery = true;
                    break;
                
                case 5: //05h
                    MBC = 2;
                    break;
                
                case 6: //06h
                    MBC = 2;
                    cartBattery = true;
                    break;
                
                case 8: //08h
                    cartRam = true;
                    break;
                
                case 9: //09h
                    cartRam = true;
                    cartBattery = true;
                    break;
                
                case 11: //0Bh
                    MMM01 = true;
                    break;
                
                case 12: //0Ch
                    MMM01 = true;
                    cartRam = true;
                    break;
                
                case 13: //0Dh
                    MMM01 = true;
                    cartRam = true;
                    cartBattery = true;
                    break;
                
                case 15: //0Fh
                    MBC = 3;
                    cartTimer = true;
                    cartBattery = true;
                    break;
                
                case 16: //10h
                    MBC = 3;
                    cartTimer = true;
                    cartRam = true;
                    cartBattery = true;
                    break;
                
                case 17: //11h
                    MBC = 3;
                    break;
                
                case 18: //12h
                    MBC = 3;
                    cartRam = true;
                    break;
                
                case 19: //13h
                    MBC = 3;
                    cartRam = true;
                    cartBattery = true;
                    break;
                
                case 21: //15h
                    MBC = 4;
                    break;
                
                case 22: //16h
                    MBC = 4;
                    cartRam = true;
                    break;
                
                case 23: //17h
                    MBC = 4;
                    cartRam = true;
                    cartBattery = true;
                    break;
                
                case 25: //19h
                    MBC = 5;
                    break;
                
                case 26: //1Ah
                    MBC = 5;
                    cartRam = true;
                    break;
                
                case 27: //1Bh
                    MBC = 5;
                    cartRam = true;
                    cartBattery = true;
                    break;
                
                case 28: //1Ch
                    MBC = 5;
                    cartRumble = true;
                    break;
                
                case 29: //1Dh
                    MBC = 5;
                    cartRumble = true;
                    cartRam = true;
                    break;
                
                case 30: //1Eh
                    MBC = 5;
                    cartRumble = true;
                    cartRam = true;
                    cartBattery = true;
                    break;
                
                case 252: //FCh
                    cartCamera = true;
                    break;
                
                case 253: //FDh
                    Tama5 = true;
                    break;
                
                case 254: //FEh
                    HuC3 = true;
                    break;
                
                case 255: //FFh
                    HuC1 = true;
                    cartRam = true;
                    cartBattery = true;
                    break;
            }
        }
    }
}
