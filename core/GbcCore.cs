using gbcNET.cpu;
using gbcNET.mbc;

namespace gbcNET.core {
    public class GbcCore {
        
        public GbcCartHeader RomHeader;

        public GbcCpu Cpu = new GbcCpu();
        public byte[] GbcMainRam = new byte[8192];
        public byte[] GbcVideoRam = new byte[8192];
        public byte[] RomCache;

        public IMemoryBankController ActiveMBC = null;
        
        public GbcCore(byte[] cartData) {
            RomCache = cartData;
            RomHeader = new GbcCartHeader(cartData);
            
            InitCore();
        }

        private void InitCore() {
            // Load requested cart components

            switch (RomHeader.MBC) {
                case 0:
                    // No MBC, no-op
                    break;
                
                case 1:
                    ActiveMBC = new MBC1();
                    break;
                
                case 2:
                    ActiveMBC = new MBC2();
                    break;
                
                case 3:
                    ActiveMBC = new MBC3();
                    break;
                
                case 4:
                    ActiveMBC = new MBC4();
                    break;
                
                case 5:
                    ActiveMBC = new MBC5();
                    break;
            }
        }
    }
}
