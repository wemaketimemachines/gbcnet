// ReSharper disable InconsistentNaming

namespace gbcNET.cpu {
    public class CpuRegisters {

        // Accumulator and flag register
        public byte[] AF = {0, 0};

        // General 16 bit registers
        public byte[] BC = {0, 0};
        public byte[] DE = {0, 0};
        public byte[] HL = {0, 0};

        // SP - Stack pointer
        public byte[] SP = {0, 0};

        // PC - Program counter, points to next memory instruction
        public byte[] PC = {0, 0};

        public ushort GetAF() { return Get16BitReg(AF); }
        public ushort GetBC() { return Get16BitReg(BC); }
        public ushort GetDE() { return Get16BitReg(DE); }
        public ushort GetHL() { return Get16BitReg(HL); }
        public ushort GetSP() { return Get16BitReg(SP); }
        public ushort GetPC() { return Get16BitReg(PC); }

        public void SetAF(ushort data) { Set16BitReg(data, out AF);}
        public void SetBC(ushort data) { Set16BitReg(data, out BC); }
        public void SetDE(ushort data) { Set16BitReg(data, out DE); }
        public void SetHL(ushort data) { Set16BitReg(data, out HL); }
        public void SetSP(ushort data) { Set16BitReg(data, out SP); }

        // Game Boy is Little endian, least significant byte first 
        private static void Set16BitReg(ushort data, out byte[] register) {
            register = new[] {(byte)(uint)(data >> 8), (byte)((uint) data & 0xFF)};
        }

        private static ushort Get16BitReg(byte[] bytes) {
            return (ushort) (bytes[1] | bytes[0] << 8);
        }
    }

    // Flag Register Quick reference
    // 7|6|5|4|3|2|1|0
    // Z|S|H|C|0|0|0|0
    // Z - Zero flag
    // S - Sub flag
    // H - Half Carry flag
    // C - Carry flag
    // Lower nibble unused
}
