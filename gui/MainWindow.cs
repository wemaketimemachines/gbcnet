using System;
using System.IO;
using System.Reflection;
using SFML.Graphics;
using SFML.Window;

namespace gbcNET {
    public class MainWindow {
        
        public VideoConfig VideoConfig { get; set; }
        public string Title { get; set; }
        
        public MainWindow(VideoConfig videoConfig, string title) {
            VideoConfig = videoConfig;
            Title = title;
        }

        public void Show() {
            
            VideoMode mode = new VideoMode(160 * VideoConfig.ScalingFactor, 144 * VideoConfig.ScalingFactor);
            RenderWindow window = new RenderWindow(mode, "gbcNET - " + Title);
            
            // window.SetVerticalSyncEnabled(CoreConfig.Vsync);
            window.SetFramerateLimit(60);

            var assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream("gbcNET.resources.gbcNETicon32.png")) {
                Image icon = new Image(stream);
                window.SetIcon(icon.Size.X,icon.Size.Y, icon.Pixels);
            }
            
            window.Closed += (obj, e) => { window.Close(); };
            window.KeyPressed +=
                (sender, e) =>
                {
                    Window thisWindow = (Window)sender;
                    if (e.Code == Keyboard.Key.Escape) {
                        thisWindow?.Close();
                    }
                };

            uint colorValue = 0;

            while (window.IsOpen) {
                window.DispatchEvents();
                
                // quick display test
                Color color = new Color {R = (byte)(colorValue * 2), G = (byte)(colorValue * 3), B = (byte)(colorValue * 3), A = 255};
                colorValue++;
                
                window.Clear(color);

                // Calculate pixels as byte array
                // Create SFML Image from byte array
                // Create SFML Texture from SFML Image
                // Create SFML Sprite from SFML Texture
                // Draw Sprite to fill window with a window.Draw() call
                
                window.Display();
            }
        }
    }
}
