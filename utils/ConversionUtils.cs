using System;

namespace gbcNET {
    public static class ConversionUtils {
        
        public static uint[] CreateLookup32() {
            var result = new uint[256];
            for (int i = 0; i < 256; i++) {
                string s = i.ToString("X2");
                result[i] = ((uint) s[0] + ((uint) s[1] << 16));
            }
            return result;
        }

        public static string ByteArrayToHexViaLookup32(byte[] bytes, uint[] lookup32) {
            
            var result = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++) {
                var val = lookup32[bytes[i]];
                result[2 * i] = (char) val;
                result[2 * i + 1] = (char) (val >> 16);
            }
            return new string(result);
        }

        public static string ByteToBinaryString(byte newByte) {
            return Convert.ToString(newByte, 2).PadLeft(8, '0');
        }

        public static bool GetBitFromByte(byte incomingByte, int byteNumber) {
            return (incomingByte & (1 << byteNumber)) != 0;

        }
    }
}
