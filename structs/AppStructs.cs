namespace gbcNET {
    public struct Vector2Int {
        public int x;
        public int y;

        public Vector2Int(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    
    public struct RGB {
        public uint r;
        public uint g;
        public uint b;

        public RGB(uint r, uint g, uint b) {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public RGB(uint greyScale) {
            r = greyScale;
            g = greyScale;
            b = greyScale;
        }
        
    }
}
