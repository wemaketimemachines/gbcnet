using System;
using System.IO;

namespace gbcNET {
    public static class GbcNetLogger {

        private static LogLevel _loggingThreshold;
        private static StreamWriter _file;

        public static void Log(string message, LogLevel logLevel) {
            if (logLevel >= _loggingThreshold) {
                Console.WriteLine(message);
                _file.WriteLine(message);
                _file.Flush();
            }
        }

        public static void LogTrace(string message) {
            Log(message, LogLevel.Trace);
        }

        public static void LogInfo(string message) {
            Log(message, LogLevel.Info);
        }

        public static void LogWarning(string message) {
            Log(message, LogLevel.Warning);
        }

        public static void LogFatal(string message) {
            Log(message, LogLevel.Fatal);
        }

        public static void SetThreshhold(LogLevel logLevel) {
            _loggingThreshold = logLevel;
        }

        public static void InitLogger(LogLevel logLevel, string logFile) {
            _file = new StreamWriter(logFile, true);
        }
    }
    
    public enum LogLevel {
        Trace,
        Info,
        Warning,
        Fatal,
        Off
    }
}